package com.group2.adds;

public interface EventListener {
    void update(String eventType);
}
