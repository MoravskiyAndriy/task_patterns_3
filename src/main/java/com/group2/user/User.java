package com.group2.user;

import com.group2.adds.EventListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class User implements EventListener {
    private static final Logger logger = LogManager.getLogger(User.class);
    private static final String FLOWER_EVENT = "flowers";
    private static final String PAPER_EVENT = "paper";
    private static final String STRING_EVENT = "strings";
    private String name;
    private Card card = Card.GENERAL;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    public void update(String eventType) {
        if (eventType.equals(FLOWER_EVENT)) {
            logger.info("{ADD}Red roses are the freshest today!{ADD}");
            logger.info("{ADD}Red tulips cost just 24.3 UAH today!{ADD}");
            logger.info("{ADD}Check out our colorful strings!{ADD}");
        }
        if (eventType.equals(PAPER_EVENT)) {
            logger.info("{ADD}Blue paper looks good with yellow flowers, remember!{ADD}");
            logger.info("{ADD}Black paper costs just 5 UAH today!{ADD}");
            logger.info("{ADD}Check out our colorful strings!{ADD}");
        }
        if (eventType.equals(STRING_EVENT)) {
            logger.info("{ADD}You have to put a few red strings in your bouquet!\n" +
                    "we really insist!{ADD}");
            logger.info("{ADD}White strings are the cheapest ones (just 3.5 UAH)!{ADD}");
            logger.info("{ADD}Check out these colorful strings!{ADD}");
        }
    }
}
