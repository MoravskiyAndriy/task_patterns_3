package com.group2;

import com.group2.adds.EventManager;
import com.group2.bouquetfactory.BouquetShop;
import com.group2.bouquetfactory.implementations.DniproBouquetShop;
import com.group2.bouquetfactory.implementations.KyivBouquetShop;
import com.group2.bouquetfactory.implementations.LvivBouquetShop;
import com.group2.bouquetsuppliers.BouquetSupplier;
import com.group2.bouquetsuppliers.BouquetType;
import com.group2.bouquetsuppliers.EventType;
import com.group2.bouquetsuppliers.bouquet.BoxingType;
import com.group2.bouquetsuppliers.bouquet.DeliveryType;
import com.group2.bouquetsuppliers.bouquet.components.*;
import com.group2.bouquetsuppliers.bouquetdecorator.BouquetDecorator;
import com.group2.bouquetsuppliers.bouquetdecorator.implementations.Boxing;
import com.group2.bouquetsuppliers.bouquetdecorator.implementations.Delivery;
import com.group2.bouquetsuppliers.implementations.CustomBouquetSupplier;
import com.group2.user.Card;
import com.group2.user.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.*;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);
    private static EventManager eventManager;
    private static final String FLOWER_EVENT = "flowers";
    private static final String PAPER_EVENT = "paper";
    private static final String STRING_EVENT = "strings";
    private static Map<Integer, FlowerType> flowersMap = new HashMap<>() {{
        put(1, FlowerType.REDROSE);
        put(2, FlowerType.WHITEROSE);
        put(3, FlowerType.ARTIFICIALROSE);
        put(4, FlowerType.REDTULIP);
        put(5, FlowerType.YELLOWTULIP);
        put(6, FlowerType.WHITETULIP);
        put(7, FlowerType.SMALLCHAMOMILE);
        put(8, FlowerType.BIGCHAMOMILE);
    }};
    private static Map<Integer, PaperType> papersMap = new HashMap<>() {{
        put(1, PaperType.WHITEPAPER);
        put(2, PaperType.BLUEPAPER);
        put(3, PaperType.REDPAPER);
        put(4, PaperType.BLACKPAPER);
    }};
    private static Map<Integer, StringType> stringsMap = new HashMap<>() {{
        put(1, StringType.REDSTRING);
        put(2, StringType.BLUESTRING);
        put(3, StringType.BLACKSTRING);
        put(4, StringType.REDSTRING);
    }};
    private static Map<Integer, BouquetShop> storeMap = new HashMap<>() {{
        put(1, new LvivBouquetShop());
        put(2, new KyivBouquetShop());
        put(3, new DniproBouquetShop());
    }};
    private static Map<Integer, EventType> eventMap = new HashMap<>() {{
        put(1, EventType.WEDDING);
        put(2, EventType.BIRTHDAY);
        put(3, EventType.VALENTINESDAY);
        put(4, EventType.FUNERALS);
    }};
    private static Map<Integer, String> weddingMap = new HashMap<>() {{
        put(1, "Angel");
        put(2, "Garden of feelings");
        put(3, "Sweet romance");
    }};
    private static Map<Integer, String> birthDayMap = new HashMap<>() {{
        put(1, "Summer love");
        put(2, "Mix");
        put(3, "Modern");
    }};
    private static Map<Integer, String> valentinesDayMap = new HashMap<>() {{
        put(1, "Classic");
        put(2, "Romantic heart");
        put(3, "With love");
    }};
    private static Map<Integer, String> funeralsMap = new HashMap<>() {{
        put(1, "Type 1");
        put(2, "Type 2");
        put(3, "Type 3");
    }};
    private static Map<Integer, BouquetType> weddingTypeMap = new HashMap<>() {{
        put(1, BouquetType.ANGEL);
        put(2, BouquetType.GARDENOFFEELINGS);
        put(3, BouquetType.SWEETROMANCE);
    }};
    private static Map<Integer, BouquetType> birthDayTypeMap = new HashMap<>() {{
        put(1, BouquetType.SUMMERLOVE);
        put(2, BouquetType.MIX);
        put(3, BouquetType.MODERN);
    }};
    private static Map<Integer, BouquetType> valentinesDayTypeMap = new HashMap<>() {{
        put(1, BouquetType.CLASSIC);
        put(2, BouquetType.ROMANTICHEART);
        put(3, BouquetType.WITHLOVE);
    }};
    private static Map<Integer, BouquetType> funeralsTypeMap = new HashMap<>() {{
        put(1, BouquetType.TYPE1);
        put(2, BouquetType.TYPE2);
        put(3, BouquetType.TYPE3);
    }};

    public static void main(String[] args) {
        eventManager = new EventManager(FLOWER_EVENT, PAPER_EVENT, STRING_EVENT);
        Scanner input = new Scanner(System.in).useLocale(Locale.US);
        logger.info("WELCOME TO OUR SHOP 'FLOWERS FOR EVERYBODY'!\nPlease enter yor name:");
        String username = input.nextLine();
        User user = new User(username);
        user.setCard(Card.GENERAL);
        eventManager.subscribe(FLOWER_EVENT, user);
        eventManager.subscribe(PAPER_EVENT, user);
        eventManager.subscribe(STRING_EVENT, user);
        logger.info("Welcome," + username + "! Please chose yor city:\nLviv-1\nKyiv-2\nDnipro-3");
        int initChoice = input.nextInt();
        chooseCatalogOrCustom(input, user, initChoice);
    }

    private static void chooseCatalogOrCustom(Scanner input, User user, int initChoice) {
        boolean choiceFlag = true;
        while (choiceFlag) {
            logger.info("Do you want to:\n1-Make custom bouquet;\n2-Choose from the catalog;");
            int typeChoice = input.nextInt();
            if (typeChoice == 2) {
                choiceFlag = catalogChoice(user, initChoice);
            } else {
                choiceFlag = customChoice(input, user);
            }
        }
    }

    private static boolean customChoice(Scanner input, User user) {
        boolean choiceFlag;
        BouquetSupplier supplier = new CustomBouquetSupplier();
        chooseFlowers(input, supplier);
        choosePapers(input, supplier);
        chooseStrings(input, supplier);
        confirmChoice(input, user, supplier);
        choiceFlag = needAnotherBouquet(input);
        supplier.resetServices();
        return choiceFlag;
    }

    private static void confirmChoice(Scanner input, User user, BouquetSupplier supplier) {
        BigDecimal servicePrice = getServicesPrice(input, user, supplier);
        logger.info("Price: " + NumberFormat.getCurrencyInstance().format(supplier.getPrice()) +
                "\n(EXTRA: " + supplier.getStringServices() + " " +
                NumberFormat.getCurrencyInstance().format(servicePrice) +
                ")\nIs this acceptable to you?(type yes/no)");
        input.nextLine();
        String iAccept = input.nextLine();
        if (iAccept.equalsIgnoreCase("yes")) {
            customProcess(supplier);
        } else {
            logger.info("We are sincerely sorry... Here, try again:");
            supplier.resetServices();
        }
    }

    private static BigDecimal getServicesPrice(Scanner input, User user, BouquetSupplier supplier) {
        BouquetDecorator decorator = new BouquetDecorator();
        decorator.setBouquet(supplier.getBouquet());
        logger.info("Boxing:\n1-Ordinary;\n2-Fascinating;\n3-BADASS GLORIOUS UNSPEAKABLE BOXING;");
        int choice = input.nextInt();
        BouquetDecorator boxing;
        boxing = getBoxingChoice(decorator, choice);
        logger.info("Delivery:\n1-Ordinary;\n2-Super fast;\n3-EXTREME SUPER MEGA FAST;");
        choice = input.nextInt();
        BouquetDecorator delivery = getDeliveryChoice(choice, boxing);
        BigDecimal servicePrice = supplier.getServicesPrice();
        if (user.getCard().equals(Card.GOLD)) {
            servicePrice = new BigDecimal(0);
        }
        return servicePrice;
    }

    private static boolean catalogChoice(User user, int initChoice) {
        boolean choiceFlag;
        Scanner catalogInput;
        catalogInput = new Scanner(System.in).useLocale(Locale.US);
        int bouquetChoice;
        BouquetShop bouquetShop = storeMap.get(initChoice);
        logger.info("Now, please, choose an event:\nWedding-1\nBirthday-2\n" +
                "Valentine's Day-3\nFunerals-4");
        int nextChoice = catalogInput.nextInt();
        logger.info("Please, choose bouquet:");
        showBouquets(nextChoice);
        bouquetChoice = catalogInput.nextInt();
        BouquetSupplier supplier = getBouquetSupplier(initChoice, bouquetChoice, bouquetShop);
        confirmCatalogChoice(user, initChoice, catalogInput, bouquetChoice, bouquetShop, supplier);
        choiceFlag = needAnotherBouquet(catalogInput);
        supplier.resetServices();
        return choiceFlag;
    }

    private static boolean needAnotherBouquet(Scanner catalogInput) {
        boolean choiceFlag = true;
        logger.info("Do you want another bouquet? (type yes/no)");
        String iWantMore = catalogInput.nextLine();
        if (iWantMore.equalsIgnoreCase("no")) {
            choiceFlag = false;
            logger.info("Good bye! :)");
        }
        return choiceFlag;
    }

    private static void confirmCatalogChoice(User user, int initChoice, Scanner catalogInput, int bouquetChoice, BouquetShop bouquetShop, BouquetSupplier supplier) {
        BigDecimal servicePrice = getServicesPrice(catalogInput, user, supplier);
        logger.info("Price: " + NumberFormat.getCurrencyInstance().format(supplier.getPrice()) +
                "\n(EXTRA: " + supplier.getStringServices() + " " +
                NumberFormat.getCurrencyInstance().format(servicePrice) +
                ")\nIs this acceptable to you?(type yes/no)");
        catalogInput.nextLine();
        String iAccept = catalogInput.nextLine();
        if (iAccept.equalsIgnoreCase("yes")) {
            bouquetShop.process(eventMap.get(initChoice), getBouquetsTypeForEvent(initChoice).get(bouquetChoice));
        } else {
            logger.info("We are sincerely sorry... Here, try again:");
            supplier.resetServices();
        }
    }

    private static void chooseStrings(Scanner input, BouquetSupplier supplier) {
        eventManager.notify(STRING_EVENT);
        boolean stringFlag = true;
        while (stringFlag) {
            logger.info("Choose string:");
            for (Map.Entry<Integer, StringType> entry : stringsMap.entrySet()) {
                logger.info(entry.getKey() + "-" + entry.getValue());
            }
            logger.info((stringsMap.size() + 1) + "-next step.");
            int choice = input.nextInt();
            if (choice > 0 && choice < stringsMap.size() + 1) {
                supplier.addAmount(new Strings(stringsMap.get(choice)), 1);
            } else {
                stringFlag = false;
            }
        }
    }

    private static void choosePapers(Scanner input, BouquetSupplier supplier) {
        eventManager.notify(PAPER_EVENT);
        boolean paperFlag = true;
        while (paperFlag) {
            logger.info("Choose paper:");
            for (Map.Entry<Integer, PaperType> entry : papersMap.entrySet()) {
                logger.info(entry.getKey() + "-" + entry.getValue());
            }
            logger.info((papersMap.size() + 1) + "-next step.");
            int choice = input.nextInt();
            if (choice > 0 && choice < papersMap.size() + 1) {
                supplier.addAmount(new Paper(papersMap.get(choice)), 1);
            } else {
                paperFlag = false;
            }
        }
    }

    private static void chooseFlowers(Scanner input, BouquetSupplier supplier) {
        eventManager.notify(FLOWER_EVENT);
        boolean flowerFlag = true;
        while (flowerFlag) {
            logger.info("Choose flowers:");
            for (Map.Entry<Integer, FlowerType> entry : flowersMap.entrySet()) {
                logger.info(entry.getKey() + "-" + entry.getValue());
            }
            logger.info((flowersMap.size() + 1) + "-next step.");
            int choice = input.nextInt();
            if (choice > 0 && choice < flowersMap.size() + 1) {
                logger.info("input amount:");
                int amount = input.nextInt();
                supplier.addAmount(new Flower(flowersMap.get(choice)), amount);
            } else {
                flowerFlag = false;
            }
        }
    }

    private static void customProcess(BouquetSupplier supplier) {
        supplier.form();
        supplier.cut();
        supplier.wrap();
        supplier.bind();
        supplier.box();
        supplier.send();
    }

    private static BouquetSupplier getBouquetSupplier(int initChoice, int bouquetChoice, BouquetShop bouquetShop) {
        return bouquetShop.
                createBouquetSupplier(eventMap.get(initChoice), getBouquetsTypeForEvent(initChoice).
                        get(bouquetChoice));
    }

    private static void showBouquets(int initChoice) {
        for (Map.Entry<Integer, String> entry : getBouquetsForEvent(initChoice).entrySet()) {
            logger.info(entry.getKey() + "- " + entry.getValue());
        }
    }

    private static BouquetDecorator getDeliveryChoice(int choice, BouquetDecorator boxing) {
        BouquetDecorator delivery;
        if (choice == 2) {
            delivery = new Delivery(DeliveryType.SUPER);
            delivery.setBouquet(boxing);
        } else if (choice == 3) {
            delivery = new Delivery(DeliveryType.EXTREME);
            delivery.setBouquet(boxing);
        } else {
            delivery = new Delivery(DeliveryType.ORDINARY);
            delivery.setBouquet(boxing);
        }
        return delivery;
    }

    private static BouquetDecorator getBoxingChoice(BouquetDecorator decorator, int choice) {
        BouquetDecorator boxing;
        if (choice == 2) {
            boxing = new Boxing(BoxingType.FASCINATING);
            boxing.setBouquet(decorator);
        } else if (choice == 3) {
            boxing = new Boxing(BoxingType.GLORIOUS);
            boxing.setBouquet(decorator);
        } else {
            boxing = new Boxing(BoxingType.ORDINARY);
            boxing.setBouquet(decorator);
        }
        return boxing;
    }

    private static Map<Integer, String> getBouquetsForEvent(int choice) {
        if (choice == 1) {
            return weddingMap;
        } else if (choice == 2) {
            return birthDayMap;
        } else if (choice == 3) {
            return valentinesDayMap;
        } else if (choice == 4) {
            return funeralsMap;
        } else {
            return birthDayMap;
        }
    }

    private static Map<Integer, BouquetType> getBouquetsTypeForEvent(int choice) {
        if (choice == 1) {
            return weddingTypeMap;
        } else if (choice == 2) {
            return birthDayTypeMap;
        } else if (choice == 3) {
            return valentinesDayTypeMap;
        } else if (choice == 4) {
            return funeralsTypeMap;
        } else {
            return birthDayTypeMap;
        }
    }
}
