package com.group2.bouquetsuppliers.bouquetdecorator;

import com.group2.bouquetsuppliers.BouquetActions;
import com.group2.bouquetsuppliers.bouquet.Bouquet;
import com.group2.bouquetsuppliers.bouquet.servicesstrategy.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class BouquetDecorator implements BouquetActions {
    private BouquetActions bouquet;
    private BigDecimal additionalPrice = new BigDecimal(0);
    private String toName = "";
    private Service additionalService;

    public void setBouquet(BouquetActions outBouquet) {
        bouquet = outBouquet;
        if (Objects.nonNull(additionalService) && Objects.nonNull(bouquet)) {
            bouquet.getServices().add(additionalService);
        }
    }

    protected void setAdditionalPrice(BigDecimal additionalPrice) {
        this.additionalPrice = this.additionalPrice.add(additionalPrice);
    }

    protected void setAdditionalService(Service additionalService) {
        this.additionalService = additionalService;
        additionalPrice = additionalPrice.add(additionalService.getPrice());
    }

    protected void setToName(String toName) {
        this.toName = toName;
    }


    @Override
    public String getName() {
        return toName;
    }

    @Override
    public BigDecimal getFinalPrice() {
        return additionalPrice;
    }

    @Override
    public List<Service> getServices() {
        return bouquet.getServices();
    }
}
