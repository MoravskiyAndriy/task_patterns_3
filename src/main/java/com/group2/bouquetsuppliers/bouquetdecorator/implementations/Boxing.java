package com.group2.bouquetsuppliers.bouquetdecorator.implementations;

import com.group2.bouquetsuppliers.bouquet.BoxingType;
import com.group2.bouquetsuppliers.bouquet.servicesstrategy.Service;
import com.group2.bouquetsuppliers.bouquet.servicesstrategy.implementations.BadassGloriousUnspeakableBoxing;
import com.group2.bouquetsuppliers.bouquet.servicesstrategy.implementations.FascinatingBoxing;
import com.group2.bouquetsuppliers.bouquetdecorator.BouquetDecorator;

public class Boxing extends BouquetDecorator {
    public Boxing(BoxingType boxingType) {
        Service service;
        if (boxingType.equals(BoxingType.FASCINATING)) {
            service = new FascinatingBoxing();
            setAdditionalPrice(service.getPrice());
            setAdditionalService(service);
            setToName(service.getName());
        } else if (boxingType.equals(BoxingType.GLORIOUS)) {
            service = new BadassGloriousUnspeakableBoxing();
            setAdditionalPrice(service.getPrice());
            setAdditionalService(service);
            setToName(service.getName());
        } else if (boxingType.equals(BoxingType.ORDINARY)) {
            setToName("Ordinary Boxing");
        }
    }
}
