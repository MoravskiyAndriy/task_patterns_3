package com.group2.bouquetsuppliers.bouquetdecorator.implementations;

import com.group2.bouquetsuppliers.bouquet.DeliveryType;
import com.group2.bouquetsuppliers.bouquet.servicesstrategy.Service;
import com.group2.bouquetsuppliers.bouquet.servicesstrategy.implementations.ExtremeSuperMegeFastDelivery;
import com.group2.bouquetsuppliers.bouquet.servicesstrategy.implementations.SuperFastDelivery;
import com.group2.bouquetsuppliers.bouquetdecorator.BouquetDecorator;


public class Delivery extends BouquetDecorator {
    public Delivery(DeliveryType deliveryType) {
        Service service;
        if (deliveryType.equals(DeliveryType.SUPER)) {
            service = new SuperFastDelivery();
            setAdditionalPrice(service.getPrice());
            setAdditionalService(service);
            setToName(service.getName());
        } else if (deliveryType.equals(DeliveryType.EXTREME)) {
            service = new ExtremeSuperMegeFastDelivery();
            setAdditionalPrice(service.getPrice());
            setAdditionalService(service);
            setToName(service.getName());
        } else if (deliveryType.equals(DeliveryType.ORDINARY)) {
            setToName("Ordinary Delivery");
        }
    }
}
