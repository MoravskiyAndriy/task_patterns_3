package com.group2.bouquetsuppliers;

import com.group2.bouquetsuppliers.bouquet.Bouquet;
import com.group2.bouquetsuppliers.bouquet.components.Component;

import java.math.BigDecimal;

public interface BouquetSupplier {
    void form();

    void cut();

    void wrap();

    void bind();

    void box();

    void send();

    void addName(String addName);

    void addAmount(Component c, int amount);

    BigDecimal getPrice();

    String getStringServices();

    BigDecimal getServicesPrice();

    void resetServices();

    Bouquet getBouquet();
}
