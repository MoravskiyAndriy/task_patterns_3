package com.group2.bouquetsuppliers.implementations;

import com.group2.bouquetsuppliers.BouquetSupplier;
import com.group2.bouquetsuppliers.bouquet.Bouquet;
import com.group2.bouquetsuppliers.bouquet.components.Component;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.stream.IntStream;

public abstract class GeneralSupplier implements BouquetSupplier {
    private final int ADDING_TIME;
    private final int BINDING_TIME;
    private final int CUTTING_TIME;
    private final int BOXING_TIME;
    private final int WRAPPING_TIME;
    private static final Logger logger = LogManager.getLogger(GeneralSupplier.class);
    protected Bouquet bouquet;

    public GeneralSupplier(int addingTime, int bindingTime, int cuttingTime, int boxingTime, int wrappingTime) {
        WRAPPING_TIME = wrappingTime;
        ADDING_TIME = addingTime;
        CUTTING_TIME = cuttingTime;
        BOXING_TIME = boxingTime;
        BINDING_TIME = bindingTime;
    }

    @Override
    public void form() {
        logger.info("Forming bouquet " + bouquet.getName() + "...");
        for (Component c : bouquet.getComponents()) {
            logger.info("Adding " + c.getName() + "...");
            try {
                Thread.sleep(ADDING_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void cut() {
        logger.info("Cutting...");
        try {
            Thread.sleep(CUTTING_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void wrap() {
        logger.info("Wrapping...");
        try {
            Thread.sleep(WRAPPING_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void bind() {
        logger.info("Binding...");
        try {
            Thread.sleep(BINDING_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void box() {
        logger.info("Boxing...");
        try {
            Thread.sleep(BOXING_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send() {
        logger.info("Your bouquet was sent.");
    }

    @Override
    public void addAmount(Component c, int amount) {
        IntStream.range(0, amount)
                .forEach(index -> {
                    bouquet.addComponent(c);
                });
    }

    @Override
    public BigDecimal getPrice() {
        return bouquet.getFinalPrice();
    }

    @Override
    public void addName(String addName) {
        bouquet.addName(addName);
    }

    @Override
    public Bouquet getBouquet() {
        return bouquet;
    }

    @Override
    public String getStringServices() {
        return bouquet.getStringServices();
    }

    @Override
    public BigDecimal getServicesPrice() {
        return bouquet.getServicesPrice();
    }

    @Override
    public void resetServices() {
        bouquet.resetServices();
    }
}
