package com.group2.bouquetsuppliers.implementations;

import com.group2.bouquetsuppliers.EventType;
import com.group2.bouquetsuppliers.bouquet.Bouquet;
import com.group2.bouquetsuppliers.bouquet.Constants;

import java.util.ArrayList;

public class CustomBouquetSupplier extends GeneralSupplier {
    public CustomBouquetSupplier() {
        super(Constants.USUAL_ADDING_TIME, Constants.USUAL_BINDING_TIME,
                Constants.USUAL_CUTTING_TIME, Constants.USUAL_BOXING_TIME,
                Constants.USUAL_WRAPPING_TIME);
        bouquet = new Bouquet(EventType.CUSTOM, new ArrayList<>());
    }
}
