package com.group2.bouquetsuppliers.implementations;

import com.group2.bouquetsuppliers.EventType;
import com.group2.bouquetsuppliers.bouquet.Bouquet;
import com.group2.bouquetsuppliers.bouquet.Constants;
import com.group2.bouquetsuppliers.bouquet.components.Flower;
import com.group2.bouquetsuppliers.bouquet.components.FlowerType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Properties;

public class ValentinesDayBouquetSupplier extends GeneralSupplier {
    private static final Logger logger = LogManager.getLogger(ValentinesDayBouquetSupplier.class);

    public ValentinesDayBouquetSupplier() {
        super(Optional.ofNullable(getProperties().
                        getProperty("VALENTINESDAY_ADDING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_ADDING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("VALENTINESDAY_BINDING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_BINDING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("VALENTINESDAY_CUTTING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_CUTTING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("VALENTINESDAY_BOXING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_BOXING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("VALENTINESDAY_WRAPPING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_WRAPPING_TIME));
        bouquet = new Bouquet(EventType.VALENTINESDAY, new ArrayList<>());
        addAmount(new Flower(FlowerType.REDROSE), 4);
        addAmount(new Flower(FlowerType.WHITEROSE), 3);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = ValentinesDayBouquetSupplier.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
