package com.group2.bouquetsuppliers.implementations;

import com.group2.bouquetsuppliers.EventType;
import com.group2.bouquetsuppliers.bouquet.Bouquet;
import com.group2.bouquetsuppliers.bouquet.Constants;
import com.group2.bouquetsuppliers.bouquet.components.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Properties;

public class BirthDayBouquetSupplier extends GeneralSupplier {
    private static final Logger logger = LogManager.getLogger(BirthDayBouquetSupplier.class);

    public BirthDayBouquetSupplier() {
        super(Optional.ofNullable(getProperties().
                        getProperty("BIRTHDAY_ADDING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_ADDING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("BIRTHDAY_BINDING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_BINDING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("BIRTHDAY_CUTTING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_CUTTING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("BIRTHDAY_BOXING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_BOXING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("BIRTHDAY_WRAPPING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_WRAPPING_TIME));
        bouquet = new Bouquet(EventType.BIRTHDAY, new ArrayList<>());
        addAmount(new Flower(FlowerType.REDTULIP), 3);
        addAmount(new Flower(FlowerType.YELLOWTULIP), 3);
        addAmount(new Flower(FlowerType.WHITETULIP), 3);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = BirthDayBouquetSupplier.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
