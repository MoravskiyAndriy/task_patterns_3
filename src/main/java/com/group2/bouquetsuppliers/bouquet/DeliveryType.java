package com.group2.bouquetsuppliers.bouquet;

public enum DeliveryType {
    SUPER, EXTREME, ORDINARY
}
