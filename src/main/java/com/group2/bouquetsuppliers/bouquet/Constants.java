package com.group2.bouquetsuppliers.bouquet;

import java.math.BigDecimal;

public class Constants {
    private Constants() {
    }

    public static final int USUAL_ADDING_TIME = 2000;
    public static final int USUAL_WRAPPING_TIME = 2000;
    public static final int USUAL_BINDING_TIME = 1000;
    public static final int USUAL_CUTTING_TIME = 1000;
    public static final int USUAL_BOXING_TIME = 1500;
    public static final BigDecimal SUPER_FAST_DELIVERY_PRICE = new BigDecimal(50);
    public static final BigDecimal EXTREME_SUPER_MEGA_FAST_DELIVERY_PRICE = new BigDecimal(100);
    public static final BigDecimal FASCINATING_BOXING_PRICE = new BigDecimal(30);
    public static final BigDecimal BADASS_GLORIOUS_UNSPEAKABLE_BOXING = new BigDecimal(60);
}
