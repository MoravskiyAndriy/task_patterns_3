package com.group2.bouquetsuppliers.bouquet;

import com.group2.bouquetsuppliers.BouquetActions;
import com.group2.bouquetsuppliers.EventType;
import com.group2.bouquetsuppliers.bouquet.components.Component;
import com.group2.bouquetsuppliers.bouquet.servicesstrategy.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Bouquet implements BouquetActions {
    private String name = "Bouquet";
    private EventType type;
    private BigDecimal price;
    private List<Component> components;
    private List<Service> services;

    public Bouquet(EventType type, List<Component> list) {
        this.type = type;
        this.components = list;
        services = new ArrayList<>();
        price = calculatePrice();
    }

    private BigDecimal calculatePrice() {
        BigDecimal somePrice = new BigDecimal(0);
        for (Component c : components) {
            somePrice = somePrice.add(c.getPrice());
        }
        return somePrice;
    }

    public void addComponent(Component c) {
        components.add(c);
        price = calculatePrice();
    }

    public void addName(String addName) {
        this.name = name + " '" + addName + "'";
    }

    public String getName() {
        return name;
    }

    public BigDecimal getFinalPrice() {
        return price.multiply(new BigDecimal(type.getValue()));
    }

    public List<Component> getComponents() {
        return components;
    }

    public List<Service> getServices() {
        return services;
    }

    public String getStringServices() {
        StringBuilder sb = new StringBuilder();
        for (Service service : services) {
            sb.append(service.getName()).append(" ");
        }
        return sb.toString();
    }

    public BigDecimal getServicesPrice() {
        BigDecimal servicesPrice = new BigDecimal(0);
        if (!services.isEmpty()) {
            for (Service service : services) {
                servicesPrice = servicesPrice.add(service.getPrice());
            }
        }
        return servicesPrice;
    }

    public void resetServices() {
        services = new ArrayList<>();
    }
}
