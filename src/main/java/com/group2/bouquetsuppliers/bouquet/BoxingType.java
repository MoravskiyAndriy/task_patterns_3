package com.group2.bouquetsuppliers.bouquet;

public enum BoxingType {
    FASCINATING, GLORIOUS, ORDINARY
}
