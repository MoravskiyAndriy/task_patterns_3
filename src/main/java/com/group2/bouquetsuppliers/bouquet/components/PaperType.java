package com.group2.bouquetsuppliers.bouquet.components;

import java.math.BigDecimal;

public enum PaperType {
    WHITEPAPER(new BigDecimal(7)), REDPAPER(new BigDecimal(8)),
    BLUEPAPER(new BigDecimal(7.5)), BLACKPAPER(new BigDecimal(5));
    private BigDecimal val;

    PaperType(BigDecimal val) {
        this.val = val;
    }

    public BigDecimal getValue() {
        return val;
    }
}
