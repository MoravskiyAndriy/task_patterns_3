package com.group2.bouquetsuppliers.bouquet.components;

import java.math.BigDecimal;

public enum ExtraComponentType {
    LVIVGIFT(new BigDecimal(0)), DNIPROGIFT(new BigDecimal(0)),
    KYIVGIFT(new BigDecimal(0));
    private BigDecimal val;

    ExtraComponentType(BigDecimal val) {
        this.val = val;
    }

    public BigDecimal getValue() {
        return val;
    }
}
