package com.group2.bouquetsuppliers.bouquet.components;

import java.math.BigDecimal;

public interface Component {
    String getName();

    BigDecimal getPrice();
}
