package com.group2.bouquetsuppliers.bouquet.components;

import java.math.BigDecimal;

public enum FlowerType {
    REDROSE(new BigDecimal(30)), WHITEROSE(new BigDecimal(34.5)), ARTIFICIALROSE(new BigDecimal(20)),
    REDTULIP(new BigDecimal(24.3)), YELLOWTULIP(new BigDecimal(25.8)), WHITETULIP(new BigDecimal(26)),
    SMALLCHAMOMILE(new BigDecimal(5)), BIGCHAMOMILE(new BigDecimal(8));
    private BigDecimal val;

    FlowerType(BigDecimal val) {
        this.val = val;
    }

    public BigDecimal getValue() {
        return val;
    }
}
