package com.group2.bouquetsuppliers.bouquet.components;

import java.math.BigDecimal;

public class Strings implements Component {
    private String name;
    private BigDecimal value;

    public Strings(StringType st) {
        this.name = st.toString().toLowerCase();
        this.value = st.getValue();
    }

    public Strings(String ss) {
        this.name = ss.toLowerCase();
        this.value = PaperType.valueOf(ss).getValue();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getPrice() {
        return value;
    }
}
