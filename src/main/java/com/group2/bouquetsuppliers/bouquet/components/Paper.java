package com.group2.bouquetsuppliers.bouquet.components;

import java.math.BigDecimal;

public class Paper implements Component {
    private String name;
    private BigDecimal value;

    public Paper(PaperType pt) {
        this.name = pt.toString().toLowerCase();
        this.value = pt.getValue();
    }

    public Paper(String ps) {
        this.name = ps.toLowerCase();
        this.value = PaperType.valueOf(ps).getValue();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getPrice() {
        return value;
    }
}
