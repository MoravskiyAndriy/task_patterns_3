package com.group2.bouquetsuppliers.bouquet.components;

import java.math.BigDecimal;

public class Flower implements Component {
    private String name;
    private BigDecimal value;

    public Flower(FlowerType ft) {
        this.name = ft.toString().toLowerCase();
        this.value = ft.getValue();
    }

    public Flower(String fs) {
        this.name = fs.toLowerCase();
        this.value = FlowerType.valueOf(fs).getValue();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getPrice() {
        return value;
    }
}
