package com.group2.bouquetsuppliers.bouquet.components;

import java.math.BigDecimal;

public class ExtraComponent implements Component {
    private String name;
    private BigDecimal value;

    public ExtraComponent(ExtraComponentType ect) {
        this.name = ect.toString().toLowerCase();
        this.value = ect.getValue();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getPrice() {
        return value;
    }
}
