package com.group2.bouquetsuppliers.bouquet.components;

import java.math.BigDecimal;

public enum StringType {
    WHITESTRING(new BigDecimal(3.5)), REDSTRING(new BigDecimal(3.8)),
    BLUESTRING(new BigDecimal(4)), BLACKSTRING(new BigDecimal(3.75));
    private BigDecimal val;

    StringType(BigDecimal val) {
        this.val = val;
    }

    public BigDecimal getValue() {
        return val;
    }
}
