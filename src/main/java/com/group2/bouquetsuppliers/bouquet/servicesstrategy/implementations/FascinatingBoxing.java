package com.group2.bouquetsuppliers.bouquet.servicesstrategy.implementations;

import com.group2.bouquetsuppliers.bouquet.Constants;
import com.group2.bouquetsuppliers.bouquet.servicesstrategy.SpecialBoxing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Properties;

public class FascinatingBoxing extends SpecialBoxing {
    private static final Logger logger = LogManager.getLogger(FascinatingBoxing.class);

    @Override
    public String getName() {
        return name + "'Fascinating boxing'";
    }

    @Override
    public BigDecimal getPrice() {
        return Optional.ofNullable(getProperties().
                getProperty("FASCINATING_BOXING_PRICE")).
                map(Double::valueOf).
                map(BigDecimal::new).
                orElse(Constants.FASCINATING_BOXING_PRICE);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = FascinatingBoxing.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
