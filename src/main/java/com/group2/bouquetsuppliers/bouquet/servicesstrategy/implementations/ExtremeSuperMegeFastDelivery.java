package com.group2.bouquetsuppliers.bouquet.servicesstrategy.implementations;

import com.group2.bouquetsuppliers.bouquet.Constants;
import com.group2.bouquetsuppliers.bouquet.servicesstrategy.Delivery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Properties;

public class ExtremeSuperMegeFastDelivery extends Delivery {
    private static final Logger logger = LogManager.getLogger(SuperFastDelivery.class);

    @Override
    public String getName() {
        return name + "'EXTREME SUPER MEGA FAST!'";
    }

    @Override
    public BigDecimal getPrice() {
        return Optional.ofNullable(getProperties().
                getProperty("EXTREME_SUPER_MEGA_FAST_DELIVERY_PRICE")).
                map(Double::valueOf).
                map(BigDecimal::new).
                orElse(Constants.EXTREME_SUPER_MEGA_FAST_DELIVERY_PRICE);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = SuperFastDelivery.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
