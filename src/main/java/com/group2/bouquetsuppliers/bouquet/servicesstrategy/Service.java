package com.group2.bouquetsuppliers.bouquet.servicesstrategy;

import java.math.BigDecimal;

public interface Service {
    String getName();

    BigDecimal getPrice();
}
