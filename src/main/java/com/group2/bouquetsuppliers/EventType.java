package com.group2.bouquetsuppliers;

public enum EventType {
    WEDDING(1.0), BIRTHDAY(1.0), VALENTINESDAY(1.0), FUNERALS(1.0), CUSTOM(1.0);
    private double val;

    EventType(double val) {
        this.val = val;
    }

    public double getValue() {
        return val;
    }
}
