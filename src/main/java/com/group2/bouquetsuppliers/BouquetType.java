package com.group2.bouquetsuppliers;

public enum BouquetType {
    ANGEL, GARDENOFFEELINGS, SWEETROMANCE,
    SUMMERLOVE, MIX, MODERN,
    CLASSIC, ROMANTICHEART, WITHLOVE,
    TYPE1, TYPE2, TYPE3
}
