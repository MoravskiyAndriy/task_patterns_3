package com.group2.bouquetsuppliers;

import com.group2.bouquetsuppliers.bouquet.servicesstrategy.Service;

import java.math.BigDecimal;
import java.util.List;

public interface BouquetActions {
    String getName();

    BigDecimal getFinalPrice();

    List<Service> getServices();
}
