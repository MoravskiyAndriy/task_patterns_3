package com.group2.bouquetfactory;

import com.group2.bouquetsuppliers.BouquetSupplier;
import com.group2.bouquetsuppliers.BouquetType;
import com.group2.bouquetsuppliers.EventType;

import java.math.BigDecimal;

public abstract class BouquetShop {
    public abstract BouquetSupplier createBouquetSupplier(EventType invent, BouquetType bouquet);

    public BigDecimal getPrice(EventType invent, BouquetType bouquet) {
        BouquetSupplier supplier = createBouquetSupplier(invent, bouquet);
        return supplier.getPrice();
    }

    public void process(EventType invent, BouquetType bouquet) {
        BouquetSupplier supplier = createBouquetSupplier(invent, bouquet);
        supplier.form();
        supplier.cut();
        supplier.wrap();
        supplier.bind();
        supplier.box();
        supplier.send();
    }
}
