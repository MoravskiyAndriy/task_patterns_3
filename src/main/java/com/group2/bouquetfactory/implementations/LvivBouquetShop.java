package com.group2.bouquetfactory.implementations;

import com.group2.bouquetfactory.BouquetShop;
import com.group2.bouquetsuppliers.BouquetSupplier;
import com.group2.bouquetsuppliers.BouquetType;
import com.group2.bouquetsuppliers.EventType;
import com.group2.bouquetsuppliers.bouquet.components.*;
import com.group2.bouquetsuppliers.implementations.*;

import java.util.HashMap;
import java.util.Map;

public class LvivBouquetShop extends BouquetShop {
    private Map<EventType, BouquetSupplier> suppliersMap = new HashMap<>();

    @Override
    public BouquetSupplier createBouquetSupplier(EventType invent, BouquetType bouquet) {
        init();
        BouquetSupplier supplier = getBirthDaySupplier(suppliersMap.get(invent), bouquet);
        if (invent.equals(EventType.WEDDING)) {
            supplier = getWeddingSupplier(suppliersMap.get(invent), bouquet);
        } else if (invent.equals(EventType.BIRTHDAY)) {
            supplier = getBirthDaySupplier(suppliersMap.get(invent), bouquet);
        } else if (invent.equals(EventType.VALENTINESDAY)) {
            supplier = getValentinesDaySupplier(suppliersMap.get(invent), bouquet);
        } else if (invent.equals(EventType.FUNERALS)) {
            supplier = getFuneralsSupplier(suppliersMap.get(invent), bouquet);
        }
        supplier.addAmount(new ExtraComponent(ExtraComponentType.LVIVGIFT), 1);
        return supplier;
    }

    private BouquetSupplier getWeddingSupplier(BouquetSupplier bs, BouquetType bouquet) {
        if (bouquet.equals(BouquetType.ANGEL)) {
            bs.addName("Angel");
            bs.addAmount(new Flower(FlowerType.YELLOWTULIP), 4);
            bs.addAmount(new Strings(StringType.REDSTRING), 1);
        }
        if (bouquet.equals(BouquetType.GARDENOFFEELINGS)) {
            bs.addName("Garden of feelings");
            bs.addAmount(new Flower(FlowerType.REDTULIP), 6);
            bs.addAmount(new Strings(StringType.BLUESTRING), 1);
        }
        if (bouquet.equals(BouquetType.SWEETROMANCE)) {
            bs.addName("Sweet romance");
            bs.addAmount(new Flower(FlowerType.REDROSE), 6);
            bs.addAmount(new Strings(StringType.REDSTRING), 1);
        }
        return bs;
    }

    private BouquetSupplier getBirthDaySupplier(BouquetSupplier bs, BouquetType bouquet) {
        if (bouquet.equals(BouquetType.SUMMERLOVE)) {
            bs.addName("Summer love");
            bs.addAmount(new Paper(PaperType.WHITEPAPER), 1);
            bs.addAmount(new Strings(StringType.REDSTRING), 1);
        }
        if (bouquet.equals(BouquetType.MIX)) {
            bs.addName("Mix");
            bs.addAmount(new Paper(PaperType.BLUEPAPER), 1);
            bs.addAmount(new Strings(StringType.BLUESTRING), 1);
        }
        if (bouquet.equals(BouquetType.MODERN)) {
            bs.addName("Modern");
            bs.addAmount(new Paper(PaperType.REDPAPER), 1);
            bs.addAmount(new Strings(StringType.REDSTRING), 1);
        }
        return bs;
    }

    private BouquetSupplier getValentinesDaySupplier(BouquetSupplier bs, BouquetType bouquet) {
        if (bouquet.equals(BouquetType.CLASSIC)) {
            bs.addName("Classic");
            bs.addAmount(new Flower(FlowerType.REDTULIP), 4);
            bs.addAmount(new Paper(PaperType.WHITEPAPER), 1);
            bs.addAmount(new Strings(StringType.REDSTRING), 1);
        }
        if (bouquet.equals(BouquetType.ROMANTICHEART)) {
            bs.addName("Romantic heart");
            bs.addAmount(new Flower(FlowerType.BIGCHAMOMILE), 8);
            bs.addAmount(new Paper(PaperType.BLUEPAPER), 1);
            bs.addAmount(new Strings(StringType.BLUESTRING), 1);
        }
        if (bouquet.equals(BouquetType.WITHLOVE)) {
            bs.addName("With love");
            bs.addAmount(new Flower(FlowerType.SMALLCHAMOMILE), 20);
            bs.addAmount(new Paper(PaperType.REDPAPER), 1);
            bs.addAmount(new Strings(StringType.REDSTRING), 1);
        }
        return bs;
    }

    private BouquetSupplier getFuneralsSupplier(BouquetSupplier bs, BouquetType bouquet) {
        if (bouquet.equals(BouquetType.TYPE1)) {
            bs.addName("Type 1");
            bs.addAmount(new Flower(FlowerType.ARTIFICIALROSE), 4);
            bs.addAmount(new Strings(StringType.REDSTRING), 1);
        }
        if (bouquet.equals(BouquetType.TYPE2)) {
            bs.addName("Type 2");
            bs.addAmount(new Flower(FlowerType.ARTIFICIALROSE), 8);
            bs.addAmount(new Strings(StringType.BLUESTRING), 1);
        }
        if (bouquet.equals(BouquetType.TYPE3)) {
            bs.addName("Type 3");
            bs.addAmount(new Flower(FlowerType.ARTIFICIALROSE), 12);
            bs.addAmount(new Strings(StringType.REDSTRING), 1);
        }
        return bs;
    }

    private void init() {
        suppliersMap = new HashMap<>();
        suppliersMap.put(EventType.WEDDING, new WeddingBouquetSupplier());
        suppliersMap.put(EventType.BIRTHDAY, new BirthDayBouquetSupplier());
        suppliersMap.put(EventType.VALENTINESDAY, new ValentinesDayBouquetSupplier());
        suppliersMap.put(EventType.FUNERALS, new FuneralsBouquetSupplier());
        suppliersMap.put(EventType.CUSTOM, new CustomBouquetSupplier());
    }
}
